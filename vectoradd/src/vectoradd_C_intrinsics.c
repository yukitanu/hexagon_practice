#include "stdint.h"

void VectorAdd(
	const uint32_t* in1,
	const uint32_t* in2,
	uint32_t* out,
	uint32_t size)
{
	uint32_t i;
	for (i = 0; i < size; i++)
	{
		out[i] = in1[i] + in2[i];
	}
}