#include "vectoradd.h"
#include "AEEStdErr.h"
#include "rpcmem.h"
#include <stdio.h>
#include <stdint.h>
#include <sys/time.h>

// Define ION heap ID (system heap) for use with HVX/Compute DSP's. 
// This is the recommended heap for use with any DSP with an SMMU, 
// which includes all cDSP's and aDSP's with HVX. (This may already 
// be defined in rpcmem.h, depending on version of that file).
#ifndef ION_HEAP_ID_SYSTEM
#define ION_HEAP_ID_SYSTEM 25
#endif

unsigned long long GetTime( void )
{
	struct timeval tv;
	struct timezone tz;

	gettimeofday(&tv, &tz);

	return tv.tv_sec * 1000000ULL + tv.tv_usec;
}

static void vectoradd_ref(
	const uint32_t* in1,
	const uint32_t* in2,
	uint32_t* out,
	uint32_t size)
{
	for (int i = 0; i < size; i++)
	{
		out[i] = in1[i] + in2[i];
	}
}

int main(int argc, char** argv)
{
	const int N = 12800;
	const int LOOPS = 10;

	uint32_t *in1 = 0, *in2 = 0;
	uint32_t *out_ref = 0;
	uint32_t *out_hex = 0;
	uint32_t size = N;

	in1 = (uint32_t*)rpcmem_alloc(ION_HEAP_ID_SYSTEM,  RPCMEM_DEFAULT_FLAGS, size * sizeof(uint32_t));
	in2 = (uint32_t*)rpcmem_alloc(ION_HEAP_ID_SYSTEM,  RPCMEM_DEFAULT_FLAGS, size * sizeof(uint32_t));
	out_ref = (uint32_t*)rpcmem_alloc(ION_HEAP_ID_SYSTEM,  RPCMEM_DEFAULT_FLAGS, size * sizeof(uint32_t));
	out_hex = (uint32_t*)rpcmem_alloc(ION_HEAP_ID_SYSTEM,  RPCMEM_DEFAULT_FLAGS, size * sizeof(uint32_t));

	// generate pseudo-random arrays
	{
		uint32_t *ptr1 = in1;
		uint32_t *ptr2 = in2;

		uint32_t m_w = 0x12349876;
		uint32_t m_z = 0xabcd4321;

		for (int i = 0; i < size; i++)
		{
			m_z = 36969 * (m_z & 65535) + (m_z >> 16);
            m_w = 18000 * (m_w & 65535) + (m_w >> 16);
			*ptr1++ = m_w % 1000;
		}

		for (int i = 0; i < size; i++)
		{
			m_z = 36969 * (m_z & 65535) + (m_z >> 16);
            m_w = 18000 * (m_w & 65535) + (m_w >> 16);
			*ptr2++ = m_w % 1000;
		}
	}

	uint64_t total_time = 0;
	for (int loop = 0; loop < LOOPS; loop++)
	{
		const uint64_t t1 = GetTime();

		vectoradd_VectorAdd(in1, size, in2, size, out_hex, size, size);

		const uint64_t t2 = GetTime();

		total_time += (t2 - t1);
	}

	vectoradd_ref(in1, in2, out_ref, size);	

	// validation
	int num_errors = 0;
	for (int i = 0; i < size; i++)
	{
		if (out_ref[i] != out_hex[i])
		{
			num_errors++;
			printf("error! (idx, expected, actual) = (%d, %d, %d)\n", i, out_ref[i], out_hex[i]);
		}
	}

	if (num_errors == 0)
	{
		printf("Success.\n");
	}
	else
	{
		printf("Failure.\n");
	}

	rpcmem_free(in1);
	rpcmem_free(in2);
	rpcmem_free(out_hex);
	rpcmem_free(out_ref);

	return 0;
}