DEVICE_PATH="/data/tmp"
HEX_ROOT_PATH = "~/Qualcomm/Hexagon_SDK/3.5.1"

adb root
adb wait-for-device
adb push hexagon_Release_toolv83_v66/ship/libvectoradd $DEVICE_PATH
adb push android_Release/ship/vectoradd $DEVICE_PATH
adb shell chmod 755 $DEVICE_PATH
adb push ${HEX_ROOT_PATH}/libs/common/apps_mem_heap/ship/hexagon_Release_dynamic_toolv83_v60/libapps_mem_heap.so $DEVICE_PATH

adb shell ${DEVICE_PATH}/vectoradd
